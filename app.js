var striptags = require('striptags');
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;
var promise = require('selenium-webdriver').promise;
var async = require('async');
var mongoose = require('mongoose');
var moment = require('moment');

mongoose.connect('mongodb://localhost/whatsapp-crawler');
var db = mongoose.connection;

db.on('error', function () {
    throw new Error('Unable to connect to database.');
});

var senderSchema = new mongoose.Schema({
    name: {type: String},
});
var Sender = mongoose.model('Sender', senderSchema);

var messageSchema = new mongoose.Schema({
    time: {type: Date},
    contents: {type: String},
    sender: {type: mongoose.Schema.ObjectId, ref: 'Sender'}
});
var Message = mongoose.model('Message', messageSchema);


// change .forBrowser to choose the browser you require eg 'chrome' 'firefox' note this requires webdriver for each browser seperately
var driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

// set refreshTime so that the duration of time in which the website is to be parsed
var refreshTime = 5 * 1000;
driver.get('http://web.whatsapp.com');

db.once('open', function () {
    function parse() {
        driver.wait(until.elementLocated(By.id('side')), 5 * 100000).then(function () {
            var newMessages = [];
            var senders = [];
            var pendingElements = driver.findElements(By.className('unread'));
            pendingElements.then(function (elements) {
                var pendingHtml = elements.map(function (elem) {
                    return elem.getAttribute("innerHTML");
                });
                promise.all(pendingHtml).then(function (allHtml) {
                    newMessages = striptags(allHtml.join()).split(',');

                    /**
                     * Regex Explanation
                     * /
                     (([0-1]{1,2}\d|\d)\:\d{1,2}\s(A|P)\M)(.*)(\d)
                     /
                     g
                     1st Capturing Group (([0-1]{1,2}\d|\d)\:\d{1,2}\s(A|P)\M)
                     2nd Capturing Group ([0-1]{1,2}\d|\d)
                     1st Alternative [0-1]{1,2}\d
                     Match a single character present in the list below [0-1]{1,2}
                     {1,2} Quantifier — Matches between 1 and 2 times, as many times as possible, giving back as needed (greedy)
                     0-1 a single character in the range between 0 (index 48) and 1 (index 49) (case sensitive)
                     \d matches a digit (equal to [0-9])
                     2nd Alternative \d
                     \d matches a digit (equal to [0-9])
                     \: matches the character : literally (case sensitive)
                     \d{1,2} matches a digit (equal to [0-9])
                     {1,2} Quantifier — Matches between 1 and 2 times, as many times as possible, giving back as needed (greedy)
                     \s matches any whitespace character (equal to [\r\n\t\f\v ])
                     3rd Capturing Group (A|P)
                     1st Alternative A
                     A matches the character A literally (case sensitive)
                     2nd Alternative P
                     P matches the character P literally (case sensitive)
                     \M matches the character M literally (case sensitive)
                     4th Capturing Group (.*)
                     .* matches any character (except for line terminators)
                     * Quantifier — Matches between zero and unlimited times, as many times as possible, giving back as needed (greedy)
                     5th Capturing Group (\d)
                     \d matches a digit (equal to [0-9])
                     Global pattern flags
                     g modifier: global. All matches (don't return after first match)
                     MATCH INFORMATION

                     */
                    async.each(newMessages,
                        function (newMessage, callback) {
                            {
                                // console.log('NEW MESSAGE - ' + newMessage);
                                if (newMessage && newMessage.trim().length > 0) {
                                    var senderName = newMessage.split(/(([0-1]{1,2}\d|\d)\:\d{1,2}\s(A|P)\M)/)[0];
                                    // console.log('SENDER - ' + senderName);
                                    var time = newMessage.split(/(([0-1]{1,2}\d|\d)\:\d{1,2}\s(A|P)\M)/)[1];
                                    // console.log('TIME - ' + time);
                                    var message = newMessage.split(/(([0-1]{1,2}\d|\d)\:\d{1,2}\s(A|P)\M)(.*)(\d)/)[4];
                                    // console.log('MESSAGE - ' + message);

                                    time = moment(time, 'h:m:a');
                                    Sender
                                        .findOneAndUpdate({name: senderName}, {}, {upsert: true, new: true})
                                        .exec(function (err, sender) {
                                            if (err) {
                                                console.log(err);
                                            }
                                            Message
                                                .update({
                                                    time: time,
                                                    contents: message,
                                                    sender: sender._id
                                                }, {}, {upsert: true})
                                                .exec(function (updateErr) {
                                                    if (updateErr) {
                                                        console.log('Failed to store message to DB.');
                                                    }
                                                    else {
                                                        console.log('Grabbed a message and saved.');
                                                    }
                                                    callback();
                                                });
                                        });
                                } else {
                                    callback();
                                }
                            }
                        },
                        function (err) {
                            console.log('Async loop completed ' + (err ? 'with error' : 'successfully'));
                        });
                });
            });
        });
        setTimeout(parse, refreshTime);
    }

    parse();
});

//driver.quit();