# Whatsapp Crawler


A whatsapp crawler based on Nodejs and Selenium.


## Requirements

  - NodeJs
  - Java
  - MongoDB
  - Selenium Drivers


## Prerequisites

* Make sure that the webdriver (gecko) is available in environment PATH
* run `java -jar -Dwebdriver.gecko.driver=geckodriver.exe selenium-server-standalone-3.4.0.jar`
* MongoDB should be running

## Running

Follow below methods to run this script:

* Clone and run `npm install`
* Start script by typing `node app.js`

## Verification
> Note - This script currently supports only text message and will not work for multimedia attachments

* Open the browser window spawned by the script and scan the QR code using whatsapp to load the messages window
* Once the messages window is loaded the new script will keep on reading the new messages and will save it to the database.